#ifndef SYSTEMPARAMS_H
#define SYSTEMPARAMS_H

#include "type.h"

#include<qjsondocument.h>
#include<qjsonarray.h>
#include<qjsonobject.h>
#include<qjsonvalue.h>
#include <QFile>
#include <QList>
#include <QMap>
#include <QDir>

#include <QTextStream>
#include <QDateTime>
#include <QMutex>
#include <QTime>

#define CACHE_TASK_NUM_MAX    128
//#define CMD_LSBLK "sudo -S lsblk -J"
//#define CMD_LSBLK   "lsblk -J"
#define CMD_LSBLK   "lsblk -J"
//#define CMD_CREATE_RAM_DISK   "modprobe brd rd_nr=1 rd_size="
#define CMD_CREATE_RAM_DISK     "insmod /root/linux-4.19-brd/tbrd.ko rd_nr=1 rd_size="
#define CMD_DELETE_RAM          "rmmod tbrd"
#define CMD_FLASH_CACHE_START   "/etc/init.d/flashcache start"
#define CMD_FLASH_CACHE_DELETE   "/etc/init.d/flashcache stop"
#define CMD_FLASH_CACHE_SUSPEND  CMD_FLASH_CACHE_DELETE
#define CMD_FLASH_CACHE_CONTINUE  CMD_FLASH_CACHE_START
#define CMD_FLASH_CACHE_STATUS   "/etc/init.d/flashcache status"

//cache task dir
#define CACHE_TASK_DIR   "cache_task"


#define BLOCK_TYPE_DISK              "disk"
#define BLOCK_TYPE_ROM               "rom"
#define BLOCK_TYPE_PART              "part"

#define CACHE_READ_STR               "加速读"
#define CACHE_WRITE_STR              "加速写"
#define CACHE_READ_WRITE_STR         "加速读写"

#define CACHE_TASK_RUNNING_STR       "激活"
#define CACHE_TASK_STOP_STR          "暂停"

namespace Ui {
class Block_Info;
class SystemParams;
class CacheStatusInfo;
}

typedef enum{
    L1_CACHE=1,
    L2_CACHE
}Cache_Type;

typedef enum{
    CACHE_READ=0,
    CACHE_READ_WRITE
}Cache_Strategy_Type;

typedef enum{
    CACHE_TASK_STOP=0,
    CACHE_TASK_RUNNING
}Cache_Task_Status;

class Block_Info
{
public:

    typedef enum{
        IS_DISK=0,
        IS_ROM,
        IS_PARTITION
    }Block_Type;

    QString m_name;
    Block_Type m_type;
    QString m_size;
    QString m_mountedPoint;
};

class CacheTaskInfo
{
public:
    QString m_taskName;
    uint16 m_taskIndex;
    QString  m_taskUuid;
    Cache_Task_Status m_taskStatus;
    Block_Info m_blockInfo;
    Cache_Type m_cacheType;
    QString m_cacheName;
    uint32 m_cacheSize;      //MB
    QString m_diskCacheSize;
    uint32 m_granularitySize;      //KB
    Cache_Strategy_Type m_cacheStrategyType;
};

class CacheStatusInfo
{
public:
    int32 reads_num=0;
    int32 writes_num=0;
    int32 reads_hit_num=0;
    int32 writes_hit_num=0;
    int8 reads_hit_percent;
    int8 writes_hit_percent;
};

class SystemParams
{
public:
    SystemParams();
    static SystemParams* instance_get();

    static void system_init();
    static QString blockTypeToString_convert(Block_Info::Block_Type type);
    static int32 jsonDocument_save(QString &path, QJsonDocument *pJsonDoc);
    static int32 cacheTaskInfoFile_save(QString &path, CacheTaskInfo *pInfo);
    static int32 jsonDocument_get(QString &path, QJsonDocument *pJsonDoc);
    static int32 cacheTaskInfoFile_get(QString &path, CacheTaskInfo *pInfo);
    static void cacheTaskDir_create();
    static void outputMessage(QtMsgType type, const QMessageLogContext &context, const QString &msg);
    static QString cacheTaskStatusToStr_convert(Cache_Task_Status status);
    static QString cacheTaskUuid_generate();
    static void cacheIdUsedArray_set(uint16 id, bool used);
    static uint16 cacheIdUsedArrayEmpty_get();

    QString m_cacheTaskPath;
    QString m_userPasswd;
    bool m_cacheIdUsedArray[CACHE_TASK_NUM_MAX];

private:
    static SystemParams* m_pInstance;

};


#endif // SYSTEMPARAMS_H
