#ifndef USERPASSWDINPUTDIALOG_H
#define USERPASSWDINPUTDIALOG_H

#include <QDialog>

namespace Ui {
class UserPasswdInputDialog;
}

class UserPasswdInputDialog : public QDialog
{
    Q_OBJECT

public:
    explicit UserPasswdInputDialog(QWidget *parent = 0);
    ~UserPasswdInputDialog();

private slots:
    void on_pushButton_cancel_clicked();

    void on_pushButton_ack_clicked();

private:
    Ui::UserPasswdInputDialog *ui;

signals:
    void mainWindowShow_signal();
};

#endif // USERPASSWDINPUTDIALOG_H
