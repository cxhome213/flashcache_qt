#include "userpasswdinputdialog.h"
#include "ui_userpasswdinputdialog.h"
#include "systemparams.h"
#include <QDebug>

UserPasswdInputDialog::UserPasswdInputDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::UserPasswdInputDialog)
{
    ui->setupUi(this);
    ui->lineEdit_passwd->setEchoMode(QLineEdit::Password);
}

UserPasswdInputDialog::~UserPasswdInputDialog()
{
    delete ui;
}

void UserPasswdInputDialog::on_pushButton_cancel_clicked()
{
    ui->label_alarm->clear();
    SystemParams::instance_get()->m_cacheTaskPath = "";
    hide();
    emit mainWindowShow_signal();
}

void UserPasswdInputDialog::on_pushButton_ack_clicked()
{
    ui->label_alarm->clear();

    QString passwd = ui->lineEdit_passwd->text();
    if (passwd.length()<=0)
    {
        QString show_str = QString::fromUtf8("密码不能为空");
        ui->label_alarm->setText(show_str);
        return;
    }

    SystemParams::instance_get()->m_userPasswd = passwd;
    hide();
    qDebug("user passwd is %s",
           SystemParams::instance_get()->m_userPasswd.toLatin1().data());
    emit mainWindowShow_signal();
}
