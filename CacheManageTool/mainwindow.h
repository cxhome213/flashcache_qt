#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QProcess>
#include <QTimer>

#include "createcachetaskdialog.h"
#include "cachetaskparamssetdialog.h"
#include "systemparams.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    QProcess *m_pShellProcess;
    QProcess *m_pShellProcessGetStatus;
    CreateCacheTaskDialog m_createCacheTaskDig;
    CacheTaskParamsSetDialog m_cacheTaskParamsSetDig;

signals:
    void createCacheTaskDigShow_signal();
    void shellProcessCmdReadOutput_signal(QString &content, QObject* from);

private slots:
    void on_pushButton_addTask_clicked();

public slots:
    void shellProcessCmdWrite_slot(QString &cmd, QObject* from);
    void cacheTaskStartOk_slot(CacheTaskInfo *pInfo);
    void mainWindowShow_slot();

private slots:
    void on_readoutput();
    void on_readerror();
    void treeItemPressed(const QModelIndex &index);
    void on_readoutput_status_get();
    void statusRefreshTimer_slot();

    void on_pushButton_deleteTask_clicked();

    void on_pushButton_suspendTask_clicked();

    void on_pushButton_continueTask_clicked();

private:
    Ui::MainWindow *ui;

    QObject *m_pShellCmdFrom;
    QStandardItemModel *m_pTreeViewModel;

    QMap<QString, CacheTaskInfo> m_cacheTaskMap;
    CacheTaskInfo *m_pCacheTaskInfoSelected;
    int32 m_treeViewRowSelected;

    QTimer m_statusRefreshTimer;

    void treeViewCacheTask_init();
    void cacheTasks_init();
    void treeViewCacheTask_add(CacheTaskInfo *pInfo);
    void cacheTaskInfo_save(CacheTaskInfo *pInfo);
    void treeViewCacheTask_update();
    void cacheTask_create(CacheTaskInfo *pInfo);
    void cacheTask_delete(QString &uuid);
    void flashCacheFile_modify(CacheTaskInfo *pInfo);
    void tableWidgetStaticStatus_fill(QString uuid);
    void shellProcessCmdGetStatus_write(QString &cmd);
    void cacheTaskStatusContent_parse(QString &content);
    void tableWidgetDynamicStatus_update(CacheStatusInfo *pInfo);
    QString readsWritesSizeToStr_convert(int32 size);
    void treeViewRow_delete(int32 row);
    void cacheTaskInfoFile_delete(QString &path);
    void pushButtonsStatus_init();
};

#endif // MAINWINDOW_H
