#ifndef CREATECACHETASKDIALOG_H
#define CREATECACHETASKDIALOG_H

#include <QDialog>
#include <QStandardItemModel>

#include "systemparams.h"

namespace Ui {
class CreateCacheTaskDialog;
}

class CreateCacheTaskDialog : public QDialog
{
    Q_OBJECT

public:
    explicit CreateCacheTaskDialog(QWidget *parent = 0);
    ~CreateCacheTaskDialog();

public slots:
    void createCacheTaskDigShow_slot();
    void shellProcessCmdReadOutput_slot(QString &content, QObject* from);

private slots:
    void treeItemChanged(QStandardItem * item);

    void on_pushButton_next_clicked();

    void on_pushButton_cancel_clicked();

signals:
    void shellProcessCmdWrite_signal(QString &cmd, QObject* from);
    void cacheTaskParamsSetDigShow_signal(QList<Block_Info> *pSelectList, QList<Block_Info> *pDiskList);

private:
    Ui::CreateCacheTaskDialog *ui;

    QStandardItemModel *m_pModel;
    QList<Block_Info> m_blockSelected;
    QList<Block_Info> m_disksList;

    void treeViewCacheTask_init();
    void blockdevicesJson_parse(QString &content);
    void treeViewCacheTask_update();
    void treeItem_checkAllChild(QStandardItem * item, bool check);
    void treeItem_checkAllChild_recursion(QStandardItem * item,bool check);
    void treeItem_CheckChildChanged(QStandardItem * item);
    Qt::CheckState checkSibling(QStandardItem * item);
    void blockCheckedList_get();
    bool blockSelectedNoEmpty_check();
};

#endif // CREATECACHETASKDIALOG_H
