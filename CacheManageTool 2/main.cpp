#include "mainwindow.h"
#include <QApplication>

#include "systemparams.h"
#include "userpasswdinputdialog.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    //注册MessageHandler
    //qInstallMessageHandler(SystemParams::outputMessage);

    UserPasswdInputDialog passwd_input_dig;
    passwd_input_dig.show();

    MainWindow w;
    w.hide();

    QObject::connect(&passwd_input_dig, SIGNAL(mainWindowShow_signal()), &w, SLOT(mainWindowShow_slot()));

    return a.exec();
}
