#ifndef CACHETASKPARAMSSETDIALOG_H
#define CACHETASKPARAMSSETDIALOG_H

#include <QDialog>
#include <QButtonGroup>

#include "systemparams.h"

namespace Ui {
class CacheTaskParamsSetDialog;
}

class CacheTaskParamsSetDialog : public QDialog
{
    Q_OBJECT

public:
    explicit CacheTaskParamsSetDialog(QWidget *parent = nullptr);
    ~CacheTaskParamsSetDialog();

public slots:
    void cacheTaskParamsSetDigShow_slot(QList<Block_Info> *pSelectList, QList<Block_Info> *pDiskList);
    //void shellProcessCmdReadOutput_slot(QString &content, QObject* from);

private slots:
    void on_pushButton_start_clicked();
    void on_pushButton_cancel_clicked();

    void on_comboBox_disks_currentIndexChanged(int index);
    void btnGroupTempLevel_slot(int index);

signals:
    void shellProcessCmdWrite_signal(QString &cmd, QObject* from);
    void cacheTaskStartOk_signal(CacheTaskInfo *pInfo);

private:
    Ui::CacheTaskParamsSetDialog *ui;

    QList<Block_Info> *m_pBlockList;
    QList<Block_Info> *m_pDiskList;
    Block_Info m_diskSelected;

    CacheTaskInfo m_cacheTaskInfo;
    QString m_ramDisk;

    QButtonGroup m_cacheTaskTypeGroup;

    void flashCacheFile_modify();
    void comboBoxDisks_init();

    void ramCacheTask_set();
    void diskCacheTask_set();
};

#endif // CACHETASKPARAMSSETDIALOG_H
