#-------------------------------------------------
#
# Project created by QtCreator 2020-05-08T16:56:37
#
#-------------------------------------------------

#QT       += core gui charts

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = CacheManageTool
TEMPLATE = app

CONFIG += c++11


SOURCES += main.cpp\
    cachetaskparamssetdialog.cpp \
        mainwindow.cpp \
    createcachetaskdialog.cpp \
    systemparams.cpp \
    testdialog.cpp \
    userpasswdinputdialog.cpp

HEADERS  += mainwindow.h \
    cachetaskparamssetdialog.h \
    createcachetaskdialog.h \
    systemparams.h \
    testdialog.h \
    type.h \
    userpasswdinputdialog.h

FORMS    += mainwindow.ui \
    cachetaskparamssetdialog.ui \
    createcachetaskdialog.ui \
    testdialog.ui \
    userpasswdinputdialog.ui

RESOURCES += \
    images.qrc
