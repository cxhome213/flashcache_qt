
#ifndef TYPE_H
#define TYPE_H

typedef signed   char      int8;
typedef unsigned char      uint8;
typedef unsigned char      byte;
typedef signed   short     int16;
typedef unsigned short     uint16;
typedef signed   int       int32;
typedef unsigned int       uint32;
typedef signed   long long int64;
typedef unsigned long long uint64;

typedef signed   char        s8;
typedef unsigned char        u8;
typedef signed   short       s16;
typedef unsigned short       u16;
typedef signed   int         s32;
typedef unsigned int         u32;
typedef signed   long long   s64;
typedef unsigned long long   u64;

#endif /* End TYPE_H */

